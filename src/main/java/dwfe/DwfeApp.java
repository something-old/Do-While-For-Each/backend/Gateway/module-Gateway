package dwfe;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@EnableEurekaClient
@SpringBootApplication
public class DwfeApp
{
  @Value("${spring.jpa.properties.hibernate.jdbc.time_zone}")
  private String timeZone;

  @PostConstruct
  void started()
  {
    TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
  }

  public static void main(String[] args)
  {
    SpringApplication.run(DwfeApp.class, args);
  }

  //
  // WORKAROUND start
  // == https://github.com/spring-cloud/spring-cloud-gateway/issues/541#issuecomment-422933041
  //
  @Bean
  public HiddenHttpMethodFilter hiddenHttpMethodFilter()
  {
    return new HiddenHttpMethodFilter()
    {
      @Override
      public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain)
      {
        return chain.filter(exchange);
      }
    };
  }
  //
  // WORKAROUND end
  //
}
